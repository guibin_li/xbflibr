{ *************************************************************************** }
{                                                                             }
{ Cartionsoft XBF file Interface                                              }
{                                                                             }
{ Java Language invoke(Read)                                                  }
{                                                                             }
{ Copyright (c) 2009, 2012 XIAOBIN                                            }
{                                                                             }
{ *************************************************************************** }

library xbfLibR;

uses
  SysUtils,
  Classes,
  JNI in 'JNI.pas',
  Udefine in 'Udefine.pas',
  Umethod in 'Umethod.pas';

{$R *.res}

// function ReadRecord(DimRecord: Integer;
//                filename1:WideString):WideString;stdcall; // original source

function ReadRecordJ(pJniEnv: PJNIEnv; pjObject: PJObject;
                        DimRecord1: JInt; filename1: JString):JString;stdcall;
var
  i: integer;
  pstr11,dbinfo:WideString;
  //
  jEnv: TJNIEnv;
  DimRecord: Integer;
  sResult: WideString;
  sReturn: JString;
begin
  //add code 2009.2.24
  jEnv := TJNIEnv.Create(pJniEnv);
  DimRecord := DimRecord1;
  //
  //FileName:=filename1; // original source
  FileName := jEnv.UnicodeJStringToString(filename1);

  if FileData.section3=0 then
  begin
    dbinfo:='True';
  end
  else
  begin
    dbinfo:='False'
  end;
  // add code
  if Not CreateFile(FileName) then
  begin
    sResult := 'not find file(read xbf - MSSQL)';
    sReturn := jEnv.StringToJString(pchar(UTF8Encode(sResult)));
    jEnv.Free;

    Result := sReturn;
    Exit;
  end;
  //
  FileStream.Seek(SizeOf(FileHead), soFromBeginning);
  If DimRecord = -1 Then
  Begin
    for i := 1 to (FileStream.Size-SizeOf(FileHead)) div SizeOf(FileData) do
    Begin
      FileStream.Read(FileData, SizeOf(FileData));
      //
      connectionstring1:=FileData.connectstr1+'='+FileData.dbprovider;
      if FileData.section6>=431205 then
      begin
        pstr11 := copy(FileData.Pstr5,1,1)
                        + copy(FileData.Pstr3,1,1)
                        + copy(FileData.Pstr4,1,1)
                        + copy(FileData.Pstr2,1,1)
                        + copy(FileData.Pstr1,1,1)
                        + copy(FileData.Pstr6,1,1);
      end;
      if FileData.section2>6 then
      begin
        connectionstring2:=FileData.connectstr2+'='+pstr11+FileData.password;
      end
      else
      begin
        connectionstring2:=FileData.connectstr2+'='+pstr11;
      end;

      connectionstring3:=FileData.connectstr3+'='+dbInfo;
      connectionstring4:=FileData.connectstr4+'='+FileData.DBuser;
      connectionstring5:=FileData.connectstr5+'='+FileData.DBname;
      connectionstring6:=FileData.connectstr6+'='+FileData.DBserver;
    End;
  End
  Else
  Begin
    FileStream.Seek(SizeOf(FileData)*DimRecord, soFromCurrent);
    FileStream.Read(FileData, SizeOf(FileData));
    //
      connectionstring1:=FileData.connectstr1+'='+FileData.dbprovider;
      if FileData.section6>=431205 then
      begin
        pstr11 := copy(FileData.Pstr5,1,1)
                        + copy(FileData.Pstr3,1,1)
                        + copy(FileData.Pstr4,1,1)
                        + copy(FileData.Pstr2,1,1)
                        + copy(FileData.Pstr1,1,1)
                        + copy(FileData.Pstr6,1,1);
      end;
      if FileData.section2>6 then
      begin
        connectionstring2:=FileData.connectstr2+'='+pstr11+FileData.password;
      end
      else
      begin
        connectionstring2:=FileData.connectstr2+'='+pstr11;
      end;
      connectionstring3:=FileData.connectstr3+'='+dbInfo;
      connectionstring4:=FileData.connectstr4+'='+FileData.DBuser;
      connectionstring5:=FileData.connectstr5+'='+FileData.DBname;
      connectionstring6:=FileData.connectstr6+'='+FileData.DBserver;
  End;

  FileStream.Free;

  //Result:=connectionstring1+connectionstring2+connectionstring3
  //      +connectionstring4+connectionstring5+connectionstring6;
  sResult := connectionstring1+connectionstring2+connectionstring3
                      +connectionstring4+connectionstring5+connectionstring6;
  sReturn := jEnv.StringToJString(pchar(UTF8Encode(sResult)));
  jEnv.Free;

  Result := sReturn;
End;
//mdb connection string
// function ReadRecord_mdb(DimRecord: Integer;
//                filename1:WideString):WideString;stdcall; // original source

function ReadRecord_mdbJ(pJniEnv: PJNIEnv; pjObject: PJObject;
                        DimRecord1: JInt; filename1: JString):JString;stdcall;
var
  i: integer;
  dbinfo:WideString;
  //
  jEnv: TJNIEnv;
  DimRecord: Integer;
  sResult: WideString;
  sReturn: JString;
begin
  //add code 2009.2.24
  jEnv := TJNIEnv.Create(pJniEnv);
  DimRecord := DimRecord1;
  //
  //FileName:=filename1; // original source
  FileName := jEnv.UnicodeJStringToString(filename1);
  
  if FileData.section3=0 then
  begin
    dbinfo:='True';
  end
  else
  begin
    dbinfo:='False'
  end;
  // add code
  if Not CreateFile(FileName) then
  begin
    sResult := 'not find file(read xbf - mdb)';
    sReturn := jEnv.StringToJString(pchar(UTF8Encode(sResult)));
    jEnv.Free;

    Result := sReturn;
    Exit;
  end;
  //
  FileStream.Seek(SizeOf(FileHead), soFromBeginning);
  If DimRecord = -1 Then
  Begin
    for i := 1 to (FileStream.Size-SizeOf(FileHead)) div SizeOf(FileData) do
    Begin
      FileStream.Read(FileData, SizeOf(FileData));
      //
      connectionstring1:=FileData.connectstr1+'='+FileData.dbprovider;
      connectionstring2:=FileData.connectstr2+'='+FileData.password;
      connectionstring3:=FileData.connectstr3+'='+dbInfo;
      connectionstring4:=FileData.connectstr4+'='+FileData.DBuser;
      connectionstring5:=FileData.connectstr5+'='+FileData.DBname;
      connectionstring6:=FileData.connectstr6+'='+FileData.DBserver;
    End;
  End
  Else
  Begin
    FileStream.Seek(SizeOf(FileData)*DimRecord, soFromCurrent);
    FileStream.Read(FileData, SizeOf(FileData));
    //
      connectionstring1:=FileData.connectstr1+'='+FileData.dbprovider;
      connectionstring2:=FileData.connectstr2+'='+FileData.password;
      connectionstring3:=FileData.connectstr3+'='+dbInfo;
      connectionstring4:=FileData.connectstr4+'='+FileData.DBuser;
      connectionstring5:=FileData.connectstr5+'='+FileData.DBname;
      connectionstring6:=FileData.connectstr6+'='+FileData.DBserver;
  End;

  FileStream.Free;

  //Result:=connectionstring1+connectionstring5+connectionstring3;
  sResult := connectionstring1+connectionstring5+connectionstring3;

  sReturn := jEnv.StringToJString(pchar(UTF8Encode(sResult)));
  jEnv.Free;

  Result := sReturn;
End;


EXPORTS
  ReadRecordJ  name 'Java_com_tdtc_xbf_ReadXbf_readRecordMsSql',
  ReadRecord_mdbJ  name 'Java_com_tdtc_xbf_ReadXbf_readRecordMdb';
  
begin
end.
 