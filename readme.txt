java 读取xbf链接文件

调用ACCESS库的函数名：
readRecordMdb
其具体的参数为:
public native String readRecordMdb(int dimRecord, String filename1);
================================
调用MSSQL库的函数名：
readRecordMsSql
其具体的参数为:
public native String readRecordMsSql(int dimRecord, String filename1);